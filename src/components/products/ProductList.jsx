import useProducts from "../../hooks/useProducts";

function ProductList() {
    const { products } = useProducts();
    console.log(products);
    
    
      if (!products) {
        return <p>Loading products...</p>;
      }
    return ( 
        <div>
        
             <ul>
                {
                  products.entries.map((p, index) => (
                    <li key={index}>
                        <span>{p.API}</span>
                    </li>
                  ))
                }
            </ul> 
        </div>
     );
}
 
export default ProductList;