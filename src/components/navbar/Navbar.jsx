import { Link } from "react-router-dom";
const Navbar = () => {
    return ( 
        <nav>
            <div>
                <div>
                    <ul>
                        <li>
                            <Link to="/">Start</Link>
                        </li>
                        <li>
                            <Link to="products" >Products</Link>
                        </li>
                        <li>
                            <Link to="/profile" >Profile</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
     );
}
 
export default Navbar;