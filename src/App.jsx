import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Navbar from './components/navbar/Navbar';
import { ROLES } from './const/roles';
import EditProductPage from './pages/EditProductPage';
import ProductPage from './pages/ProductPage';
import ProfilePage from './pages/ProfilePage';
import StartPage from './pages/StartPage';
import KeycloakRoute from './routes/KeycloakRouter';

function App() {
  return (
    <BrowserRouter>
     <Navbar/>
     <main className="container">
      <Routes>
        <Route path="/" element={<StartPage />} />
        <Route path="/products/:id" element={<EditProductPage />} />
          <Route path="/products" element={<ProductPage />} />
          <Route path="/profile" 
          element={
            <KeycloakRoute role={ROLES.Admin}>
          <ProfilePage />
            </KeycloakRoute>

          } />
      </Routes>
     </main>
    </BrowserRouter>
    
  );
}

export default App;
