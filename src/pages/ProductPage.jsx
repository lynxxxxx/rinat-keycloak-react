import ProductList from "../components/products/ProductList";

const ProductPage = () => {
    return ( 
        <div>
            <h1>Product Page</h1>
            <ProductList />
        </div>
     );
}
 
export default ProductPage;