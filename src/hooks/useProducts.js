import { useEffect, useState } from "react";
import { fetchProducts } from "../api/products";

/**
 * Make an HTTP Request and return response
 * @returns {{products:[], error: string | null }}
 */
function useProducts() {
  const [products, setProducts] = useState();
  const [error, setError] = useState("");
  const productsURL = "https://api.publicapis.org/entries";


// useEffect(
//     () => async () => {
//       try {
//         fetch(productsURL)
//           .then((res) => res.json())
//           .then((data) =>
//             setProducts({
//               entries: data.entries,
//             })
//           );
//       } catch (error) {
//         console.log(error.message);
//       }
//     },
//     []
//   );



  useEffect(() => {
    const init = async () => {
        try{
      const  res = await fetch(productsURL);
      const data = await res.json()
      console.log(data);
     
      setProducts(data)
      console.log(products);
        }

   catch (error) {
 console.log(error.message)
      }
      
    }
    init();
  }, []);

  return { products, error };
}
export default useProducts;